import {User} from './User';

//Clase Plantilla
export class Product{
    public name: String;
    public price: Number;
    public user: User;
    public description: String;
    public productType: String
    public imgURL: String;
    public imgCarrousel: String[];
    

    constructor(name: String, price: Number, user: User, desc: String, productType: String, imgURL: String, imgCarrousel: String[]){
        this.name = name;
        this.price = price;
        this.user = user;
        this.description = desc;
        this.productType = productType;
        this.imgURL = imgURL;
        this.imgCarrousel = imgCarrousel;
    }
}