//Clase Plantilla
export class User{
    public userName: String;
    public userPass: String;
    public userEmail: String
    public userPhoneNumber: Number;
    public userIMG: String
    

    constructor(name: String, pass: String, email: String, number: Number, imgURL: String){
        this.userName = name;
        this.userPass = pass;
        this.userEmail = email;
        this.userPhoneNumber = number;
        this.userIMG = imgURL;
    }
}