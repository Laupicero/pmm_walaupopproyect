import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


//Componentes
import { ProductItemListComponent } from './Components/product-item-list/product-item-list.component';
import { MenuNavBarComponent } from './Components/menu-nav-bar/menu-nav-bar.component';
import { MenuLoginComponent } from './Components/menu-login/menu-login.component';
import { ProductTemplateComponent } from './Components/product-template/product-template.component';

//Páginas
import { LoginPage } from './Pages/login-page/login-page.component';
import { ProductListPage } from './Pages/product-list-page/product-list-page.component';
import { ProductDetailsPage } from './Pages/product-details-page/product-details-page.component';

//Componentes de AngularMaterial que necesitamos
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';

//Para usar FlexBox
import { FlexLayoutModule } from '@angular/flex-layout';

//Para el Carrousel de imágenes
import { IvyCarouselModule } from 'angular-responsive-carousel';

//Para la validacion de los formularios
import { ReactiveFormsModule} from '@angular/forms';

//Para GoogleMaps
import { AgmCoreModule } from '@agm/core';  // No lo implemento al final

//Pagos con Paypal
import { NgxPayPalModule } from 'ngx-paypal';

//Para hacer peticiones
import { HttpClientModule } from '@angular/common/http';
import { MenuSideNavContainerComponent } from './Components/menu-side-nav-container/menu-side-nav-container.component';




@NgModule({
  declarations: [
    AppComponent,
    MenuLoginComponent,
    LoginPage,
    ProductListPage,
    ProductItemListComponent,
    MenuNavBarComponent,
    ProductTemplateComponent,
    ProductDetailsPage,
    MenuSideNavContainerComponent,
  ],
  imports: [
    BrowserModule, AppRoutingModule, BrowserAnimationsModule, FlexLayoutModule,
    MatButtonModule, MatInputModule,MatFormFieldModule, MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule,
    ReactiveFormsModule, HttpClientModule, MatCardModule, IvyCarouselModule, NgxPayPalModule,
    AgmCoreModule.forRoot({
      apiKey: ''
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
