import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/Models/Interface/Product';
import { ProductService } from 'src/app/Services/product.service';

declare var paypal;

@Component({
  selector: 'app-product-details-page',
  templateUrl: './product-details-page.component.html',
  styleUrls: ['./product-details-page.component.css']
})
export class ProductDetailsPage implements OnInit {
  public product: Product;
  showSuccess: boolean ;

  @ViewChild('paypal', { static : true}) paypalElement: ElementRef;
  // Google-maps
  // public lat: number;
  // public lng: number;
  // public zoom: number;
  // public mapTypeID: String;


  //Constructor
  constructor(private location: Location, 
    private router: Router, 
    private routeAct: ActivatedRoute, 
    private productService: ProductService) {  
      // Variables para usar el googleMaps
      // this.lat = 40;
      // this.lng = -3;
      // this.zoom = 6;
      // this.mapTypeID = `hybrid`;
     }


  // OnInit
  ngOnInit(): void {
    this.getProduct();
    this.initConfig();
  }

  // Obtenemos el producto
  getProduct() {
    let id: number = Number(this.routeAct.snapshot.paramMap.get('numProduct'));
    this.productService. getProducts()
      .subscribe(products => this.product = products[id]);
  }

// Para la config de 'pàso atrás'
  goBack(): void {
    this.location.back();
  }


  // Para el PayPal
  private initConfig(): void {
   paypal.Buttons(
     {
       createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units:[
              {
                description: this.product.name,
                amount: {
                  currency_code: 'EUR',
                  value: this.product.price
                }
              }
            ]
          })
       },
       onApprove: async (data, actions) => {
         const order = await actions.order.capture();
         console.log(order);
       },
       onError: err =>{
         console.log(err);
       }
     }
   ).render(this.paypalElement.nativeElement);
  }
}

