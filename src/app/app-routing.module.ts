import { NgModule } from '@angular/core';
//RouterModule es el módulo donde está el sistema de rutas
//Routes es una declaración de un de tipo, que corresponde con un array de objetos Route
import { Routes, RouterModule } from '@angular/router';

//Importamos los modulos/componentes que harán de las diferentes páginas
import { LoginPage } from './Pages/login-page/login-page.component';
import { ProductListPage } from './Pages/product-list-page/product-list-page.component';
import { ProductDetailsPage } from './Pages/product-details-page/product-details-page.component';

//Los 'guards'
import {AuthLoginGuard} from './Guards/auth-login.guard';


const routes: Routes = [
  { path: '', component: LoginPage},
  { path: 'login', component: LoginPage },
  { path: 'products', component: ProductListPage, canActivate: [AuthLoginGuard]},
  { path: 'products/numberProduct/:numProduct', component: ProductDetailsPage}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
