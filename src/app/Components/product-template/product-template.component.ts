import { Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from '../../Models/Interface/Product';


@Component({
  selector: 'product-component',
  templateUrl: './product-template.component.html',
  styleUrls: ['./product-template.component.css']
})
export class ProductTemplateComponent{
  @Input() product: Product;
  @Output() productDetails = new EventEmitter<Product>();


  goToDetailsPage(){
    this.productDetails.emit(this.product);

  }
}
