import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/Models/Interface/Product';
import { ProductService } from 'src/app/Services/product.service';
import { debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';


@Component({
  selector: 'product-item-list',
  templateUrl: './product-item-list.component.html',
  styleUrls: ['./product-item-list.component.css']
})
export class ProductItemListComponent implements OnInit {
  public productList: Product[];

  // Injectamos 'ProductService' para obtener todos los productos y el 'Router' para navegar
  constructor(private productService: ProductService, private router: Router) {}

  ngOnInit(): void {
    this.getAllProducts();       
  }
  

  sendProductIndexAndNavigate(product){
    this.router.navigate(['/products/numberProduct', this.productList.indexOf(product)]);      
  }

  // Obtiene todos los productos a través del servicio
  getAllProducts(): void{
    this.productService.getProducts().subscribe(products => this.productList = products);
  }

}
