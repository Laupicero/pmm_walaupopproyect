import { Component} from '@angular/core';

@Component({
  selector: 'menu-side-nav-container',
  templateUrl: './menu-side-nav-container.component.html',
  styleUrls: ['./menu-side-nav-container.component.css']
})
export class MenuSideNavContainerComponent{
  public isExpanded: boolean = true;
  public isShowing: boolean = false;
  public showSubmenu: boolean = false;
  public showSubSubMenu: boolean = false;

  constructor() { }

  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }


   //--------------------------------------
  // Para mostrarnos los diferentes tipos de productos
  //--------------------------------------

  // Nos muestra todos los productos
  showAllProducts(){

  }

  // Productos de decoración
  showDecorationProducts(){

  }

  // Productos de tecnología
  showTecnologyProducts(){

  }

  // Productos - Libros
  showBooksProducts(){

  }

  // Productos - Objetos diversos
  showStuffProducts(){

  }

// Productos Favoritos del usuario
  showCustomFavoriteProducts(){

  }

}
