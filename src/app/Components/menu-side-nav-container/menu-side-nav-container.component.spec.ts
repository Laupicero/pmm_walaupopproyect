import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSideNavContainerComponent } from './menu-side-nav-container.component';

describe('MenuSideNavContainerComponent', () => {
  let component: MenuSideNavContainerComponent;
  let fixture: ComponentFixture<MenuSideNavContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuSideNavContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSideNavContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
