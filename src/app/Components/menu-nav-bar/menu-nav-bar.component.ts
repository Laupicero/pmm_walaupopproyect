import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Models/Interface/User';
import { UserLoginService } from 'src/app/Services/user-login.service';

@Component({
  selector: 'menu-navBar-component',
  templateUrl: './menu-nav-bar.component.html',
  styleUrls: ['./menu-nav-bar.component.css']
})
export class MenuNavBarComponent {
  public isExpanded: boolean = true;
  public user: User;

  constructor(private userLoginService: UserLoginService) {
    this.user = userLoginService.currentUser;
  }

  
//TODO: Para 'deslogear al usuario'
// Usaremos otro 'guards'
  unlogUser(){ }

  
 
}
