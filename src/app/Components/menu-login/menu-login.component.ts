import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserLoginService } from 'src/app/Services/user-login.service';

@Component({
  selector: 'menuLogin-component',
  templateUrl: './menu-login.component.html',
  styleUrls: ['./menu-login.component.css']
})
export class MenuLoginComponent {
  //Var para el formGroup
  formLogin: FormGroup;
  public loginStatusUser: String;
  public loginStatusPass: String;

  constructor(private formBuilder: FormBuilder, private userLoginService: UserLoginService, private router: Router) {
    this.buildForm();
  }

  
  //------------------
  //Métodos
  //------------------

  //Nos agrupa los datos del formulario
  buildForm() {
    this.formLogin = this.formBuilder.group({
      userNameFCN: ['', [Validators.required, Validators.minLength(4)]],
      userPassFCN: ['', [Validators.required, Validators.minLength(5)]]
    });
    this.loginStatusUser = `succesStatus`;
    this.loginStatusPass = `succesStatus`;
  }

  //Reseteamos los valores
  resetStatus(){
    if(this.loginStatusUser == `errorStatus`){
      this.loginStatusUser = `succesStatus`;
      this.formLogin.get('userNameFCN').reset();
    }

    if(this.loginStatusPass == `errorStatus`){
      this.loginStatusPass = `succesStatus`;
      this.formLogin.get('userPassFCN').reset();
    }
  }
  

  //Veríficamos todos los datos del usuario
  verifyUser(){
    if(this.formLogin.valid){
      if(this.userLoginService.checkRegisteredUserData( this.formLogin.get('userNameFCN').value, this.formLogin.get('userPassFCN').value)){
        this.router.navigate(['/products']);
      }else{
        alert(`Not registered user\n Use: 'User' - '123456'`);
      }

    }else{  //Comprobamos uno por uno los errores

      //Nombre Usuario
      if(this.formLogin.get('userNameFCN').touched || this.formLogin.get('userNameFCN').errors ){
        this.loginStatusUser = `errorStatus`;
        // Campo Requerido
        if(this.formLogin.get('userNameFCN').hasError('required'))
         this.formLogin.controls['userNameFCN'].setValue(`Field Required`);
        //Mínimo 5 caracteres
        if(this.formLogin.get('userNameFCN').hasError('minlength'))
          this.formLogin.controls['userNameFCN'].setValue(`4 characters minium`);     
      }

      //Contraseña
      if(this.formLogin.get('userPassFCN').touched || this.formLogin.get('userPassFCN').errors ){
        this.loginStatusPass = `errorStatus`;
        // Campo Requerido
        if(this.formLogin.get('userPassFCN').hasError('required'))
        this.formLogin.controls['userPassFCN'].setValue(`Field Required`);
        //Mínimo 5 caracteres
        if(this.formLogin.get('userPassFCN').hasError('minlength'))
        this.formLogin.controls['userPassFCN'].setValue(`5 characters minium`);        
      }
    }
  }

}
 