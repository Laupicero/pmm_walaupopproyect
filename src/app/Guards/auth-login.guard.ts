import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserLoginService } from '../Services/user-login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthLoginGuard implements CanActivate {

  //Cosntructor al que le inyectaremos el servicio
  constructor(private ulService: UserLoginService, private router: Router) { }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.ulService.isAuthenticated()) {
      return true;
    }else{
    // not logged
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }//End canActivate
  
}
