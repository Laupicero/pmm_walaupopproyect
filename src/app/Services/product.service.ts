import { HttpClient } from '@angular/common/http';
import { Injectable} from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Product } from '../Models/Interface/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public apiURL: String = `assets/Data/DAO/ProductsWalaupop.json`;

  constructor(private request: HttpClient) {
    this.getProducts();
    
   }

  //--------------------
  // MÉTODOS
  //--------------------
  // public getProductByIndex(index): Product{
  //   return this.products[index];
  // }

  //Nos devuelve en formato 'JSON' todos los Productos
  public getProducts(): Observable<Product[]> {
    return this.request.get<Product[]>(`${this.apiURL}`)
    .pipe(
      tap(_ => this.log('fetched Products')),
      catchError(this.handleError<Product[]>('getProducts', []))
    );    
  }



  // Handle Http operation that failed.
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  /** Log a Product-Service message with the MessageService */
  private log(message: string) {
    console.log(`ProductService: ${message}`);
  }
}
