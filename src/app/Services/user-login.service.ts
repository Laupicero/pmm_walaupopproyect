import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../Models/Interface/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {
  public userRegistered: Boolean;
  public currentUser: User;
  public walaupopUsers: User[];
  public apiURL: String = `assets/Data/DAO/usersWalaupop.json`;
  

  constructor(private request: HttpClient) {  
    this.userRegistered = false;
    // console.log(this.userRegistered);

    this.getJSONData().subscribe(data => {
      this.walaupopUsers = data;
      //console.log(this.walaupopUsers);
  });
  }

  //Nos busca en nuestra 'DAO - DB' si el usuario existe 
  checkRegisteredUserData(userName: any, userPass: any): Boolean {
    for (let user of this.walaupopUsers){
      if(user.userName == userName && user.userPass == userPass){
        this.userRegistered = true;
        this.currentUser = user;
      }
    }
    // console.log(this.userRegistered);
    return this.userRegistered;
  }


  //Nos devuelve en formato 'JSON' todos los usuarios
  public getJSONData(): Observable<any> {
    return this.request.get(`${this.apiURL}`);
}


  // Nos devuelve si el ususario esta autenticado
  isAuthenticated() {
    return this.userRegistered;
  }
}
